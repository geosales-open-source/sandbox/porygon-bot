const dotenvsafe = require("dotenv-safe")
const Discord = require("discord.js")

dotenvsafe.config()

const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] })

client.on("ready", () => {
  console.log(`Loguei como ${client.user.tag}!`)
})

client.on("message", msg => {
    if (msg.content.startsWith(";porygon ")) {
        const exibir = msg.content.split(" ").slice(1)
        console.log(exibir)
        msg.reply(exibir.join(" "))
    }

    else if (msg.content.startsWith(";hello ")) {
        const repeticao = Number(msg.content.split(" ")[1])
        if (repeticao == 0) {
            msg.reply("Tchau, Porygon !")
        } else {
            msg.reply(`;hello ${repeticao-1}`)
        }
    }
    
    else if (msg.content.startsWith(";help")) {
        msg.reply(`
            Bem vindo ao Help:
            porygon <msg>--> Exibe msg
            hello <n>--> Cria um loop de <n> iterações e, após isso, desliga Porygon`
        )
     }
})

client.login(process.env.DISCORD_TOKEN)